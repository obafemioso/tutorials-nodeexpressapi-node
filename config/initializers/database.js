var mongoose  = require('mongoose');
var config    = require('nconf');

module.exports = function(cb) {
  mongoose.connect(config.get('database').server);

  if (cb) {
    return cb();
  }
};