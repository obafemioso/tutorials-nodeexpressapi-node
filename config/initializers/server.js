// BASE SETUP
// =================================================

// call the packages we need
var express     = require('express');   // call express
var path        = require('path');
// Local dependencies
var config      = require('nconf');

// create the express app
// configure middlewares
var bodyParser  = require('body-parser');
var app;

var start = function(cb) {
  'use strict';
  // Configure express
  app = express();

  // configure app to use bodyParser()
  // this will let us get the data from a POST
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  console.log('[SERVER] Initializing routes');
  require('../../app/routes/index')(app);

  // Serve up our static compiled assets from /public
  app.use(express.static(path.join(__dirname, 'public')));

  // Error handler
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: (app.get('env') === 'development' ? err : {})
    });
    next(err);
  });

  // START THE SERVER
  app.listen(config.get('NODE_PORT'));
  console.log('[SERVER] Listening on port ' + config.get('NODE_PORT'));

  if (cb) {
    return cb();
  }
};

module.exports = start;