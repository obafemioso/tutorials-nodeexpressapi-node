var nconf = require('nconf');

nconf.set('url', 'lochalhost');

nconf.set('database', {
  user: 'username',
  password: 'password',
  server: 'mongodb://localhost:27017/node-api'
});