var changeCase  = require('change-case');
var express     = require('express');
var routes      = require('require-dir')();

module.exports = function(app) {
  'use strict';

  var router = express.Router();

  router.get('/', function(req, res) {
    res.json({ message: 'Hooray! Welcome to our API!' });
  });

  app.use('/api', router);

  // Initialize all routes
  Object.keys(routes).forEach(function(routeName) {
    var router = express.Router();
    // You can add some middleware here
    router.use(function(req, res, next) {
      // do logging
      console.log('Something is happening.');
      next(); // make sure we go to the next routes and don't stop here
    });

    // Initialize the route to add its functionality to router
    require('./' + routeName)(router);

    // Add router to the specified route name in the app
    // all of our routes will be prefixed with /api
    app.use('/api/' + changeCase.paramCase(routeName), router);
  });
};