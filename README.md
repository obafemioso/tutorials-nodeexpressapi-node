# Node Express REST API Tutorial #

[Tutorial](https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4)

### Setup ###

* run `npm install` to install from package.json
* `node index.js` starts the api (Note: expects local mongo db at localhost:27017/node-api)
* go to `localhost:8080/api` to verify api started successfully

### Endpoints ###

* /api/bears
	* GET - return all bears
	* POST - saves a bear with given name. Params: name
* /api/bears/:id
	* GET - get bear with id :id
	* PUT - update bear with id :id with given name. Params: name
	* DELETE - delete bear with id :id